/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./app/**/*.{js,ts,jsx,tsx}",
        "./pages/**/*.{js,ts,jsx,tsx}",
        "./components/**/*.{js,ts,jsx,tsx}",

        // Or if using `src` directory:
        "./src/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        extend: {
            backgroundImage: {
                "hero-image2": "url('/cow2.png')",
            },
            fontFamily: {
                kaushan_Script: [`kaushan_Script`],
                Prompt_Regular: [`Prompt_Regular`],
                Roboto_Regular: [`Roboto_Regular`],
                Roboto_Medium: [`Roboto_Medium`],
                Point_Regular: [`Point_Regular`],
                Title_Regular: [`Title_Regular`],
                Merriweather_Regular: [`Merriweather_Regular`],
                Merriweather_Italic: [`Merriweather_Italic`],
            },
        },
    },
    plugins: [require("daisyui")],
};
