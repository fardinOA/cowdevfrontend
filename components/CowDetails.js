import moment from "moment";
import Link from "next/link";
import React, { useState } from "react";
import AddEvents from "./AddEvents";

const CowDetails = ({ cow }) => {
    const [showEvents, setShowEvents] = useState(false);

    return (
        <div className=" min-h-screen">
            <div className="  flex justify-center container text-white p-4 mx-auto ">
                <div>
                    <div className="flex justify-between">
                        <div className="text-sm breadcrumbs mb-4 bg-black w-fit px-4 rounded-md bg-opacity-50 font-kaushan_Script text-[tomato]   ">
                            <ul>
                                <li>
                                    <Link href={`/`}>Home</Link>
                                </li>
                                <li>
                                    <a>{cow.name}</a>
                                </li>
                            </ul>
                        </div>

                        {/* <button className="text-sm cursor-pointer breadcrumbs mb-4 bg-black w-fit px-4 rounded-md bg-opacity-50 font-kaushan_Script text-[tomato] "> */}
                        <label
                            className="text-sm cursor-pointer breadcrumbs mb-4 bg-black w-fit px-4 rounded-md bg-opacity-50 font-kaushan_Script text-[tomato]"
                            htmlFor="my-modal-4"
                        >
                            {" "}
                            Add Event
                        </label>
                        {/* </button> */}
                    </div>
                    <div className="   flex flex-col gap-6 w-[90vw] md:w-[70vw] lg:w-[50vw]">
                        <div className="bg-black bg-opacity-60 p-4 rounded-md   ">
                            <table className="table-fixed w-full">
                                <thead>
                                    <tr className=" text-[tomato] text-[1.2rem] font-kaushan_Script ">
                                        <th className="">Name</th>
                                        <th>Age</th>
                                        <th>Color</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="text-center">
                                            {cow?.name}
                                        </td>
                                        <td className="text-center">
                                            {cow?.age}
                                        </td>
                                        <td className="text-center">
                                            {cow?.color}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div className="bg-black bg-opacity-60 p-4 rounded-md   ">
                            <div className="flex justify-between">
                                <h1 className=" text-[1.3rem]  text-[tomato] font-Merriweather_Regular ">
                                    Events
                                </h1>
                                {showEvents ? (
                                    <svg
                                        onClick={() =>
                                            setShowEvents(!showEvents)
                                        }
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        strokeWidth="1.5"
                                        stroke="currentColor"
                                        className="w-6 h-6 text-[tomato] cursor-pointer "
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            d="M4.5 15.75l7.5-7.5 7.5 7.5"
                                        />
                                    </svg>
                                ) : (
                                    <svg
                                        onClick={() =>
                                            setShowEvents(!showEvents)
                                        }
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        strokeWidth="1.5"
                                        stroke="currentColor"
                                        className="w-6 h-6 text-[tomato] cursor-pointer "
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            d="M19.5 8.25l-7.5 7.5-7.5-7.5"
                                        />
                                    </svg>
                                )}
                            </div>
                            {showEvents && (
                                <div className="flex flex-col gap-4">
                                    {cow?.events?.map((ele, ind) => (
                                        <div
                                            className=" p-2 rounded-md border-opacity-60 border-white border "
                                            key={ind}
                                        >
                                            <h1 className="text-center  shadow-white text-[tomato] capitalize border-b w-fit mx-auto ">
                                                {ele.eventName}
                                            </h1>
                                            <p className="p-2 text-center bg-black mt-2 rounded-md font-Roboto_Regular bg-opacity-60">
                                                {ele.description}
                                            </p>

                                            <div className="flex justify-between p-2 ">
                                                <p className="bg-[tomato] p-1 rounded-md">
                                                    {moment
                                                        ?.duration(
                                                            moment().diff(
                                                                ele.startDate
                                                            )
                                                        )
                                                        .humanize() + " passed"}
                                                </p>
                                                <p className="bg-[tomato] p-1 rounded-md">
                                                    {moment
                                                        ?.duration(
                                                            moment().diff(
                                                                ele.endDate
                                                            )
                                                        )
                                                        .humanize() + " To Go"}
                                                </p>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
            <AddEvents cow={cow} />
        </div>
    );
};

export default CowDetails;
