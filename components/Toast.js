import React from "react";

const Toast = () => {
    return (
        <div className="toast toast-top toast-start">
            <div className="alert alert-info">
                <div>
                    <span>New mail arrived.</span>
                </div>
            </div>
            <div className="alert alert-success">
                <div>
                    <span>Message sent successfully.</span>
                </div>
            </div>
        </div>
    );
};

export default Toast;
