import axios from "axios";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { toast } from "react-toastify";
import { defaultUrl } from "./apiUrl";

const AddEvents = ({ cow }) => {
    const router = useRouter();
    const [eventName, setEventName] = useState("");
    const [startDate, setStartDate] = useState("");
    const [endDate, setEndDate] = useState("");
    const [description, setDescription] = useState("");

    const submitHandeler = async (e) => {
        e.preventDefault();
        const submitData = {
            cowId: cow?._id,
            eventName,
            startDate,
            endDate,
            description,
        };

        console.log(submitData);

        try {
            axios.defaults.withCredentials = true;
            const config = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Headers": "*",
                },
            };

            const { data } = await axios.post(
                `${defaultUrl}/cowdev/addEvent`,
                submitData,
                config
            );
            console.log("add event", data);
            if (data?.success) {
                toast.success(data?.message, {
                    position: "top-center",
                    autoClose: 2000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "light",
                });
                setTimeout(() => {
                    router.reload();
                }, 2500);
            }
        } catch (error) {
            console.log(error);

            toast.error(error?.response?.data?.error?.message, {
                position: "top-center",
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
            });
        }
    };
    return (
        <div>
            <input type="checkbox" id="my-modal-4" className="modal-toggle" />
            <label
                htmlFor="my-modal-4"
                className="modal mx-auto cursor-pointer"
            >
                <label className="modal-box relative" htmlFor="">
                    <h3 className="text-[1.5rem]   text-center border-b font-Title_Regular font-extrabold ">
                        Add Your Event
                    </h3>
                    <div className="py-4">
                        <form className="flex flex-col gap-8 " action="">
                            <div className="flex flex-col">
                                <label
                                    className=" text-[1.2rem] font-kaushan_Script "
                                    htmlFor=""
                                >
                                    Event Name
                                </label>
                                <input
                                    onChange={(e) =>
                                        setEventName(e.target.value)
                                    }
                                    type="text"
                                    className=" text-black font-Merriweather_Regular border-2 border-black w-full h-8 rounded-md p-2  "
                                />
                            </div>
                            <div className="flex flex-col">
                                <label
                                    className=" text-[1.2rem] font-kaushan_Script "
                                    htmlFor=""
                                >
                                    Start Date
                                </label>
                                <input
                                    onChange={(e) =>
                                        setStartDate(e.target.value)
                                    }
                                    type="date"
                                    className="border-2  font-Merriweather_Regular border-black w-full h-8 rounded-md p-2   "
                                />
                            </div>
                            <div className="flex flex-col">
                                <label
                                    className=" text-[1.2rem] font-kaushan_Script "
                                    htmlFor=""
                                >
                                    End Date
                                </label>
                                <input
                                    onChange={(e) => setEndDate(e.target.value)}
                                    type="date"
                                    className="border-2  font-Merriweather_Regular border-black w-full h-8 rounded-md p-2   "
                                />
                            </div>
                            <div className="flex w-full flex-col">
                                <label
                                    className=" text-[1.2rem] font-kaushan_Script "
                                    htmlFor=""
                                >
                                    Description
                                </label>
                                <textarea
                                    onChange={(e) =>
                                        setDescription(e.target.value)
                                    }
                                    className="file-input text-black p-4 focus:outline-none border-black border-2 w-full  "
                                />
                            </div>

                            <div className="modal-action">
                                <button
                                    htmlFor="my-modal-4"
                                    onClick={submitHandeler}
                                    className="border-2 font-kaushan_Script cursor-pointer transition-all duration-300  p-2   w-full   rounded-md  bg-black  text-white   "
                                >
                                    Add
                                </button>
                            </div>
                        </form>
                    </div>
                </label>
            </label>
        </div>
    );
};

export default AddEvents;
