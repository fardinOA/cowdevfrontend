import axios from "axios";
import Image from "next/image";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import useSWR from "swr";
import { defaultUrl } from "./apiUrl";
import Loader from "./Loader";

// const fetcher = (url) => axios.get(url).then((res) => res.data);
const AllCow = () => {
    const [cows, setCows] = useState([]);

    // const { data, error } = useSWR(
    //     "http://localhost:9090/cowdev/cows",
    //     fetcher
    // );
    const loadCow = async () => {
        try {
            axios.defaults.withCredentials = true;
            const { data } = await axios.get(`${defaultUrl}/cowdev/cows`);

            setCows(data?.cows);
        } catch (error) {
            console.log(error?.response?.data?.message);
        }
    };

    useEffect(() => {
        // setCows(data?.cows);
        loadCow();
    }, []);

    return (
        <div className="  mx-auto gap-8 transition-all duration-300 grid grid-cols-1 md:grid-cols-2 py-8  ">
            {cows?.length > 0 ? (
                cows?.map((ele, ind) => (
                    <Link href={`/cow/${ele._id}`} key={ele._id}>
                        <div className=" w-[23rem]      text-white bg-black bg-opacity-70 shadow-2xl shadow-black cursor-pointer hover:scale-105 transition-all duration-300 rounded-[10px] p-4 ">
                            <div className="w-full h-[12rem] ">
                                <Image
                                    alt=""
                                    src={ele.avatar.url}
                                    height={100}
                                    width={100}
                                    layout="responsive"
                                    className="w-full h-full object-fit rounded-t-md"
                                />
                            </div>
                            <div className="flex justify-between gap-2 py-2">
                                <p className="text-[1.5rem] truncate  w-[12rem]  bg-black px-2  font-kaushan_Script rounded-md bg-opacity-50 ">
                                    {ele.name}
                                </p>
                                <p className="text-[1.5rem]  text-center    bg-black px-2  font-kaushan_Script rounded-md bg-opacity-50 ">
                                    {ele.age} years
                                </p>
                            </div>
                            {/* <Timer /> */}
                            <div className="h-[10rem]  overflow-hidden border-[tomato] border-2 rounded-md p-2 border-opacity-50   ">
                                Lorem ipsum dolor sit amet consectetur
                                adipisicing elit. Adipisci alias vero nisi
                                repellat sequi ratione praesentium molestias qui
                                officia nemo nostrum officiis quisquam aliquid
                                dicta perspiciatis quo, dolore veritatis
                                blanditiis nihil unde modi. Ex rem, maiores ab
                                possimus blanditiis, dicta atque sequi vero
                                nobis beatae pariatur, iste impedit dolorum.
                                Optio! Lorem ipsum dolor sit amet consectetur
                                adipisicing elit. Adipisci alias vero nisi
                                repellat sequi ratione praesentium molestias qui
                                officia nemo nostrum officiis quisquam aliquid
                                dicta perspiciatis quo, dolore veritatis
                                blanditiis nihil unde modi. Ex rem, maiores ab
                                possimus blanditiis, dicta atque sequi vero
                                nobis beatae pariatur, iste impedit dolorum.
                                Optio!
                            </div>
                        </div>
                    </Link>
                ))
            ) : (
                <Loader />
            )}
        </div>
    );
};

export default AllCow;
