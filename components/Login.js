import axios from "axios";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { defaultUrl } from "./apiUrl";

const Login = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const router = useRouter();

    const loadUser = async () => {
        try {
            axios.defaults.withCredentials = true;
            const { data } = await axios.get(`${defaultUrl}/cowdev/loadUser`);
            // console.log("data", data);
            if (data.success) router.push("/");
        } catch (error) {
            // console.log(error);
            // console.log(error?.response?.data?.message);
            toast.error(error?.response?.data?.message, {
                position: "top-center",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
            });
        }
    };
    useEffect(() => {
        loadUser();
    }, []);

    const submitHandeler = async (e) => {
        e.preventDefault();
        try {
            const config = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Headers": "*",
                },
            };
            axios.defaults.withCredentials = true;
            const res = await axios.post(
                `${defaultUrl}/cowdev/login`,
                {
                    email,
                    password,
                },
                config
            );
            if (res.data.success) router.push("/");
        } catch (error) {
            // console.log("error", error);
        }
    };
    return (
        <div className="h-screen w-screen flex justify-center items-center  ">
            <div className="   ">
                <div className="mockup-phone bg-black bg-opacity-50">
                    <div className="camera  "></div>
                    <div className="display">
                        <div className="artboard artboard-demo phone-1 ">
                            <form className="flex flex-col gap-8 " action="">
                                <div className="flex flex-col">
                                    <label
                                        className=" text-[1.2rem] font-kaushan_Script "
                                        htmlFor=""
                                    >
                                        Email
                                    </label>
                                    <input
                                        onChange={(e) =>
                                            setEmail(e.target.value)
                                        }
                                        type="email"
                                        className=" font-Merriweather_Regular border-2 border-black w-full h-8 rounded-md p-2  "
                                    />
                                </div>
                                <div className="flex flex-col">
                                    <label
                                        className=" text-[1.2rem] font-kaushan_Script "
                                        htmlFor=""
                                    >
                                        Password
                                    </label>
                                    <input
                                        onChange={(e) =>
                                            setPassword(e.target.value)
                                        }
                                        type="password"
                                        className="border-2  font-Merriweather_Regular border-black w-full h-8 rounded-md p-2   "
                                    />
                                </div>
                                <div>
                                    <input
                                        onClick={submitHandeler}
                                        className="border-2 font-kaushan_Script cursor-pointer transition-all duration-300     w-full h-8 rounded-md  bg-black  text-white   "
                                        type="submit"
                                    />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;
