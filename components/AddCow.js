import axios from "axios";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { toast } from "react-toastify";
import { defaultUrl } from "./apiUrl";
const AddCow = () => {
    const router = useRouter();
    const [name, setName] = useState("");
    const [age, setAge] = useState(0);
    const [color, setColor] = useState("");
    const [image, setImage] = useState("");
    const submitHandeler = async (e) => {
        e.preventDefault();

        try {
            const cowData = { name, age, color, image };
            axios.defaults.withCredentials = true;
            const config = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Headers": "*",
                },
            };

            const { data } = await axios.post(
                `${defaultUrl}/cowdev/addCow`,
                cowData,
                config
            );
            console.log("add Cow", data);
            if (data?.success) {
                toast.success(data?.message, {
                    position: "top-center",
                    autoClose: 2000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "light",
                });
                setTimeout(() => {
                    router.reload();
                }, 2500);
            }
        } catch (error) {
            console.log(error);

            if (error?.response?.data?.error?.code == 11000) {
                toast.error("This Name you used already", {
                    position: "top-center",
                    autoClose: 3000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "light",
                });
            }
        }
    };

    const imageHandeler = (e) => {
        e.preventDefault();
        // const reader = new FileReader();
        // reader.onload = () => {
        //     if (reader.readyState === 2) {
        //         setImage(reader.result);
        //     }
        // };
        // reader.readAsDataURL(e.target.files[0]);

        let file = e.target.files[0];
        if (file) {
            let reader = new FileReader();
            reader.onloadend = () => {
                setImage(reader.result);
            };
            reader.readAsDataURL(file);
        }
    };
    return (
        <div>
            <input type="checkbox" id="my-modal-4" className="modal-toggle" />
            <label
                htmlFor="my-modal-4"
                className="modal mx-auto cursor-pointer"
            >
                <label className="modal-box relative" htmlFor="">
                    <h3 className="text-[1.5rem]   text-center border-b font-Title_Regular font-extrabold ">
                        Add your Favourite Cow
                    </h3>
                    <div className="py-4">
                        <form className="flex flex-col gap-8 " action="">
                            <div className="flex flex-col">
                                <label
                                    className=" text-[1.2rem] font-kaushan_Script "
                                    htmlFor=""
                                >
                                    Cow Name
                                </label>
                                <input
                                    onChange={(e) => setName(e.target.value)}
                                    type="text"
                                    className=" font-Merriweather_Regular border-2 border-black w-full h-8 rounded-md p-2  "
                                />
                            </div>
                            <div className="flex flex-col">
                                <label
                                    className=" text-[1.2rem] font-kaushan_Script "
                                    htmlFor=""
                                >
                                    Age
                                </label>
                                <input
                                    onChange={(e) => setAge(e.target.value)}
                                    type="number"
                                    className="border-2  font-Merriweather_Regular border-black w-full h-8 rounded-md p-2   "
                                />
                            </div>
                            <div className="flex flex-col">
                                <label
                                    className=" text-[1.2rem] font-kaushan_Script "
                                    htmlFor=""
                                >
                                    Color
                                </label>
                                <input
                                    onChange={(e) => setColor(e.target.value)}
                                    type="text"
                                    className="border-2  font-Merriweather_Regular border-black w-full h-8 rounded-md p-2   "
                                />
                            </div>
                            <div className="flex flex-col">
                                <label
                                    className=" text-[1.2rem] font-kaushan_Script "
                                    htmlFor=""
                                >
                                    Image
                                </label>
                                <input
                                    onChange={(e) => imageHandeler(e)}
                                    type="file"
                                    className="file-input w-full max-w-xs"
                                />
                            </div>

                            <div className="modal-action">
                                <button
                                    htmlFor="my-modal-4"
                                    onClick={submitHandeler}
                                    className="border-2 font-kaushan_Script cursor-pointer transition-all duration-300  p-2   w-full   rounded-md  bg-black  text-white   "
                                >
                                    Add
                                </button>
                            </div>
                        </form>
                    </div>
                </label>
            </label>
        </div>
    );
};

export default AddCow;
