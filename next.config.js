/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    images: {
        domains: ["*", "beta.ctvnews.ca", "res.cloudinary.com"],
    },
};

module.exports = nextConfig;
