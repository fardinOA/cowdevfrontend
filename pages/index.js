import Head from "next/head";
import Image from "next/image";
import { Inter } from "next/font/google";
import { useEffect, useState } from "react";

import axios from "axios";
import Navbar from "@/components/Navbar";
import AddCow from "@/components/AddCow";
import { useRouter } from "next/router";
import AllCow from "@/components/AllCow";
import { defaultUrl } from "@/components/apiUrl";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
    const router = useRouter();

    const loadUser = async () => {
        try {
            axios.defaults.withCredentials = true;
            const { data } = await axios.get(`${defaultUrl}/cowdev/loadUser`);
            // console.log("data", data);
        } catch (error) {
            // console.log(error?.response?.data?.message);
            router.push("/login");
        }
    };
    useEffect(() => {
        loadUser();
    }, []);

    return (
        <>
            <Head>
                <title>Cow Dev</title>
            </Head>
            <main className="       h-screen w-screen  overflow-scroll ">
                <Navbar />
                <div className=" grid items-center  ">
                    <AllCow />
                </div>
                <AddCow />
            </main>
        </>
    );
}
