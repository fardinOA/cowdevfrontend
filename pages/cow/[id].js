import { defaultUrl } from "@/components/apiUrl";
import CowDetails from "@/components/CowDetails";
import axios from "axios";
import React from "react";

const cow = ({ data }) => {
    return <CowDetails cow={data.cow} />;
};

export async function getServerSideProps(context) {
    try {
        axios.defaults.withCredentials = true;
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*",
            },
            withCredentials: true,
            headers: {
                Cookies: context.req.cookies.cowdevtoken,
            },
        };
        axios.AxiosHeaders = context.req.cookies;
        const res = await axios.get(
            `${defaultUrl}/cowdev/cow/${context.params.id}`,
            config
        );

        return { props: { data: res.data } };
    } catch (error) {
        // console.log(error);
        return {
            redirect: {
                permanent: false,
                destination: `/`,
            },
        };
    }
}

export default cow;
